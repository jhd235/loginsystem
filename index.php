<?php
    session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login System</title>
    </head>
    <body>
        <form action="login.php" method="post">
            <input type="text" name="uid" placeholder="User Name"><br>
            <input type="password" name="pwd" placeholder="Password"><br>
            <button type="submit">Log in</button><br><br>
        </form>
        <?php
            if (isset($_SESSION['id'])){
                echo $_SESSION['id'];
            } else {
                echo 'You are not logged in';
            }
        ?>
        <br><br><br>
        <form action="signup.php" method="post">
            <input type="text" name="first" placeholder="First Name"><br>
            <input type="text" name="last" placeholder="Last Name"><br>
            <input type="text" name="uid" placeholder="User Name"><br>
            <input type="password" name="pwd" placeholder="Password"><br>
            <button type="submit">Sign Up</button><br>
        </form>
        <br>
        <form action="logout.php" method="post">
            <button type="submit">Log out</button>
        </form>
    </body>
</html>